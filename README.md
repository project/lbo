# Layout Bulk Operations

Provides functionality for working with Layout blocks and batch api.


## Installation

Enable the module and flush Drupal cache.


## Usage

Go to 'Admin -> Structure -> Layout Bulk Add Block' and fill the form out.
