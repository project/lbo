<?php

namespace Drupal\layout_bulk_operations\Form;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\SectionComponent;
use Drupal\node\Entity\Node;

/**
 * Form to add block to layout nodes using batch api.
 */
class LayoutBulkAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'layout_bulk_operations_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // check for user permission?
    if (!\Drupal::currentUser()->hasPermission('administer layout bulk operations')) {
      return;
    }

    $entityFieldManager = \Drupal::service('entity_field.manager');
    $contentTypes = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    $form['default_custom_block'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'block_content',
      '#title' => $this->t('Default block to add to all nodes'),
      '#description' => $this->t('Create new blocks for this form in "Admin -> Structure -> Block Layout -> Custom Block Layout"'),
      '#tags' => TRUE,
      '#weight' => '0',
    ];
    $form['bulk_settings'] = [
      '#type' => 'details',
      '#title' => t('Bulk Settings'),
      '#open' => true,
      '#description' => t('Once you choose a default block, decide what the node selection rules are here.')
    ];
    $checkBoxes = [];
    foreach ($contentTypes as $key => $value) {
      // check if ct has layout_builder__layout field
      $fields = $entityFieldManager->getFieldDefinitions('node', $key);
      if (isset($fields['layout_builder__layout'])) {
        $checkBoxes[$key] = $value->label();
      }
    }
    $form['bulk_settings']['content_types'] = array(
      '#title' => t('Layout enabled content types'),
      '#description' => t('Choose nodes of one or more content types.'),
      '#type' => 'checkboxes',
      '#options' => $checkBoxes
    );
    $form['bulk_settings']['block_section'] = array(
      '#title' => t('What section to update'),
      '#type' => 'textfield',
      '#size' => 4,
      '#description' => t('Enter a number for what section to add a block or words "first" or "last".'),
      '#value' => '1'
    );
    $form['bulk_settings']['block_placement'] = array(
      '#title' => t('What spot to add new block in section'),
      '#description' => t('Enter a number or the word "first" or "last" for what spot to add a block.'),
      '#type' => 'textfield',
      '#size' => 10
    );
    $form['bulk_settings']['node_count'] = array(
      '#title' => t('Amount nodes to update'),
      '#description' => t('Enter a number or the word "all".'),
      '#type' => 'textfield',
      '#size' => 4,
      '#default_value' => '4'
    );
    $form['submit_button'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Add layout block to nodes'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Query all blog posts and loop.
    $query = \Drupal::entityQuery('node')
      ->condition('type', $values['content_types'], 'IN');
    // add optional range query
    if ($values['node_count'] != 'all') {
      $range = (int) $values['node_count'];
      $query->range(0, $range);
    }
    $nids = $query->execute();

    // Add one node per batch operation.
    $blockVars = [
      'block_id' => $values['default_custom_block'],
      'block_placement' => $values['block_placement'],
      'block_section' => $values['block_section']
    ];
    $operations = [];
    foreach ($nids as $nid) {
      $operations[] = [
        'Drupal\layout_bulk_operations\Form\LayoutBulkAddForm::updateNode',
        [$nid, $blockVars]
      ];
    }
    $batch = [
      'title' => $this->t('Updating nodes ...'),
      'operations' => $operations,
      'finished' => 'Drupal\layout_bulk_operations\Form\LayoutBulkAddForm::finishBatch',
    ];
    batch_set($batch);
  }

  /**
   * Batch operation start.
   *
   * @param $nid
   * @param $context
   *
   * @return void
   */
  public static function updateNode($nid, $blockVars, &$context){
    $node = Node::load($nid);
    $layout = $node->layout_builder__layout;
    $sectionCount = $layout->count();
    $sections = $layout->getValue();

    // Figure out what section to update.
    switch ($blockVars['block_section']) {
      case 'first':
        $section = $layout->first()->getValue();
        $sectionIndex = 1;
        break;
      case 'last':
        $sectionIndex = $sectionCount - 1; // array index
        $section = $sections[$sectionIndex]['section'];
        break;
      default:
        // get n-th section
        $sectionIndex = (int) $blockVars['block_section'] - 1; // array index
        $section = $sections[$sectionIndex]['section'];
        break;
    }

    // @todo figure out if $delta is a zero index or +1 is needed
    switch ($blockVars['block_placement']) {
      case 'first':
        $delta = 1;
        break;
      case 'last':
        // Get count of components and add one for the new one.
        $delta = count($section->getComponents()) + 1;
        break;
      default:
        // get n-th slot
        $delta = (int) $blockVars['block_placement'];
        break;
    }

    // Create clone of default block for each node:
    $blockId = $blockVars['block_id'][0]['target_id'];
    $defaultBlock = BlockContent::load($blockId);
    $blockType = 'inline_block:' . $defaultBlock->bundle();

    // Try using duplicate.
    $duplicateBlock = $defaultBlock->createDuplicate();
    $duplicateBlock->setNonReusable();
    $duplicateBlock->save();
    $blockRevId = $duplicateBlock->getRevisionId();

    // Add new block to a layout section component.
    $config = [
      'label' => '',
      'provider' => 'layout_builder',
      'label_display' => 0,
      'view_mode' => 'full',
      'block_revision_id' => $blockRevId,
      'block_serialized' => null,
      'context_mapping' => []
    ];
    // @todo dynamically set region instead of hard code 'first' ???
    $component = new SectionComponent(
      \Drupal::service('uuid')->generate(),
      'first',
      [
        'id' => $blockType
      ] + $config);
    // Place new block section in the proper slot.
    if ($blockVars['block_placement'] === 'last') {
      $section->appendComponent($component);
    } else {
      $section->insertComponent($delta, $component);
    }

    // Add or update node section, then save node.
    $node->layout_builder__layout[$sectionIndex]->setValue($section);
    $node->save();
    $context['results'][] = $nid;
  }

  /**
   * Batch finish.
   *
   * @param $success
   * @param $results
   * @param $operations
   *
   * @return void
   */
  public static function finishBatch($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $ids = implode(', ', $results);
      $message = \Drupal::translation()->formatPlural(
          count($results),
          'One batch operation processed.', '@count operations processed.'
        ) . ' [' . $ids . ']';
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
    \Drupal::logger('layout bulk operations')->info($message);
  }
}
